# Taurus MapReduce Block in FPGA

This repository contains the source code and instructions for building an FPGA-based implementation
of the Taurus's MapReduce block. For more details, please read our [Taurus: A Data Plane Architecture for Per-Packet ML]()
paper (to appear in ASPLOS '22). 

Next, we list the steps on how to clone, setup, and use this repository.

> **Note:** we assume the following OS and tools pre-installed on the machine:
> - Ubuntu 18.04.2 LTS
> - Xilinx Vivado 2020.2
> - Docker 

## Clone the MapReduce repository

Clone the `taurus/mapreduce` repository.

```sh
cd ~
git clone https://gitlab.com/dataplane-ai/taurus/mapreduce.git
cd mapreduce
git submodule init
git submodule update
```

This will clone the repository and also initialize the (dependent) 
[Spatial](https://github.com/stanford-ppl/spatial) and 
[Xilinx Open-NIC Shell](https://github.com/Xilinx/open-nic-shell) repositories as submodules.

## Configure the environment, licences, and dependencies

### Taurus MapReduce

Append the following lines in your `~/.bashrc` file.

```sh
export MAPREDUCE_ROOT_DIR=/home/<your-username>/<path-to-mapreduce-repo>
```

> **Note:** please remember to update `<your-username>` and `<path-to-mapreduce-repo>` with 
your username and path on the host machine.

### Spatial DSL

First, install the following dependencies:    

* [Scala SBT](http://www.scala-sbt.org/)
* [Java JDK 8](http://www.oracle.com/technetwork/java/javase/downloads/index.html) 
* Integer Set Library (sudo apt-get install pkg-config libgmp3-dev libisl-dev)
* Python and Scapy

> **Note:** you can use the following gist to install these on Ubuntu 18.04.2 LTS: 
[`https://gitlab.com/snippets/1987558`](https://gitlab.com/snippets/1987558)

Next, append the following line in your `~/.bashrc` file.

```sh
export _JAVA_OPTIONS="-Xmx10g -Xss8912k -Xms4g"
```

This will set the Java heap to appropriate sizes, needed for compiling Spatial programs.

## Description of Taurus MapReduce block

The following figure shows the high-level architecture of the Taurus MapReduce block. 
For more details on each of these 
components (or subsystems), please read details on the Xilinx Open-NIC Shell, 
[here](https://github.com/Xilinx/open-nic-shell).

<img src="doc/mapreduce-block.png" alt="The MapReduce block design." width="600">

The important thing to note is that, as a programmer, we only need to specify the logic 
for `MapReduce` (e.g., a loopback or more complicated logics in Spatial). The `MapReduce` 
block connects to CMAC and PCIe over their respective master/slave AXI  stream interfaces. 
For register IO, the `MapReduce` block connects to the local `Registers` module, which 
further connects to the `System Config` subsystem over an AXI-Lite interface.

## Programming the Taurus MapReduce block

### a. Build a Spatial program

Run the following commands:

```sh
cd ~/mapreduce
export SCALA_PROGRAM=Loopback512UnitTests.scala; \
export SCALA_CLASS=Loopback512; \
make build-spatial
```

This will compile the `Loopback512` program in the 
`$MAPREDUCE_ROOT_DIR/app/src/Loopback512UnitTests.scala` file. The results will be located in 
the `$MAPREDUCE_ROOT_DIR/build/mapreduce/spatial` directory. 

> **Note:** 
> 1. Ignore `simv` errors at the end of compilation.
> 2. If build fails, try resetting the cache by running `make build-spatial-rc`
> 3. To clean spatial build, run `make clean-spatial`

### b. Generate an FPGA bitstream

Run the following commands:

```sh
cd ~/mapreduce
make build-fpga
```

This will generate a bitstream under `$MAPREDUCE_ROOT_DIR/deps/open-nic/build` directory, 
which we can use to program the FPGA.

> **Note:** to clean FPGA build, run `make clean-fpga`

### c. Simulate the FPGA design

For simulation, we replace the CMAC QSFP interfaces with AXIS (mater/slave) interfaces that 
connect to a TAP interface in Linux, which is then used to send packets to/from MapReduce 
via Scapy in Python. The following figure shows the simulation setup.

<img src="doc/mapreduce-sim.png" alt="The MapReduce simulation setup." width="600">

Build the simulation files as follows:

```sh
cd ~/mapreduce
make build-fpga-sim
```

Complete the next steps from within a Linux Desktop (e.g., GNOME or Xfce).

> **Note:** 
> - We need to run the simulation (i.e., Vivado GUI) as `root`, as it requires spinning up the Linux 
TAP interface to interact with the MapReduce design.
> - Use VNC for remote access to the Linux Desktop.

Become `root` user.

```sh
sudo -i 
# enter your user password here
```

Setup a Linux TAP interface, named `tap0`.

```sh
ip tuntap add mode tap dev tap0
ip link set tap0 up
```

Open the project in the Vivado GUI.

```sh
vivado <path-to-mapreduce-repo>/deps/open-nic/build/au250_mapreduce/open_nic_shell/open_nic_shell.xpr
```

Click `Run Simulation` in the Flow Navigator. Add the desired signals to the waveform, and 
then click `run all` (the play button).

> **Note:** when running for the first time, Vivado will export certain IP files for simulation. This may take some time.

#### Sending and receiving packets

The `tap0` interface connects to CMAC in the MapReduce design. To inject a packet into the 
simulation, simply write a packet to the `tap0` interface. For example, run the following 
instructions in a Scapy shell:

```python
>>> from scapy.all import *
>>> pkt = Ether(dst="08:11:22:33:44:08", src="08:55:66:77:88:08")/IP()/TCP()/'THIS IS A TEST PKT!!'
>>> sendp(pkt, iface="tap0")
```

> **Note:** 
> 1. You can run Scapy as a docker instance using the following command:
> ```sh
> sudo $MAPREDUCE_ROOT_DIR/script/docker/scapy
> ```
> 2. To track and debug the packets in a MapReduce design, see the waveform in the Vivado GUI.

You can use `tcpdump` to view the sent/receiced packets over the `tap0` interface. 

#### Reading and writing registers

For reading RX and TX counters over the PCIe interface use the following steps. In a spearate 
shell (as root), run:

```sh
sudo -i
echo "read_addr 0x0B000" >> /tmp/cli_commands.txt  # for TX packet count.
echo "read_addr 0x0B020" >> /tmp/cli_commands.txt  # for RX packet count.
```

The output is displayed in the Vivado GUI's Tcl Console tab. 

For writes the format is: `echo "write_addr <address> <value>" >> /tmp/cli_commands.txt`.

#### Cleaning up ...

Stop the Vivado simulation and delete the `tap0` interface (use sudo).

```sh
ip link set tap0 down
ip tuntap del mode tap dev tap0
```

> **Note:** to clean the FPGA simulation build, run `make clean-fpga-sim`

Have fun!

## Contact Us 
- [Tushar Swamy](https://www.linkedin.com/in/tushar-swamy-b4aa51b1/)
- [Alexander Rucker](https://www.linkedin.com/in/acrucker/)
- [Annus Zulfiqar](https://annusgit.github.io/)
- [Muhammad Shahbaz](https://mshahbaz.gitlab.io/)
