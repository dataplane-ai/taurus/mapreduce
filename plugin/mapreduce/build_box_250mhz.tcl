# *************************************************************************
#
# Copyright 2021 Tushar Swamy (Stanford University),
#                Alexander Rucker (Stanford University),
#                Annus Zulfiqar (Purdue University),
#                Muhammad Shahbaz (Stanford/Purdue University)
# Copyright 2020 Xilinx, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# *************************************************************************
set CLOCK_FREQ_HZ 250000000

read_verilog -quiet -sv mapreduce_250mhz.sv

set spatial_dir ./spatial
if {![file exists ${spatial_dir}]} {
    puts "ERROR: ${spatial_dir} does not exist"
    return
}
read_verilog -quiet [glob -nocomplain -directory ${spatial_dir} "*.v"]
read_verilog -quiet -sv [glob -nocomplain -directory ${spatial_dir} "*.sv"]
read_vhdl -quiet [glob -nocomplain -directory ${spatial_dir} "*.vhdl"]

source ${spatial_dir}/bigIP.tcl
