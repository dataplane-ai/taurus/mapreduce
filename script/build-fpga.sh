#!/usr/bin/env bash

# *************************************************************************
#
# Copyright 2021 Tushar Swamy (Stanford University),
#                Alexander Rucker (Stanford University),
#                Annus Zulfiqar (Purdue University),
#                Muhammad Shahbaz (Stanford/Purdue University)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# *************************************************************************

PWD=$(pwd)

if [ -z "${MAPREDUCE_ROOT_DIR}" ]; then
  echo "MAPREDUCE_ROOT_DIR environment variable is not set."
  exit
fi

BOARD=au250
PLUGIN=mapreduce

SIM_DIR=${SIM_DIR:-"${MAPREDUCE_ROOT_DIR}"/sim}
BUILD_DIR=${BUILD_DIR:-"${MAPREDUCE_ROOT_DIR}"/build}

OPENNIC_REPO_PATH=${OPENNIC_REPO_PATH:-"${MAPREDUCE_ROOT_DIR}"/deps/open-nic}

arg_bitstream=0

while [[ "$#" -gt 0 ]]; do
    case $1 in
        --simulation) arg_simulation=1 ;;
        --bitstream) arg_bitstream=1 ;;
        --clean) arg_clean=1 ;;
	      --clean_all) arg_clean_all=1 ;;
        *) echo "Unknown argument: $1"; exit 1 ;;
    esac
    shift
done

clean_build () {
    rm -rf "${OPENNIC_REPO_PATH}"/sim
    sudo rm -rf "${OPENNIC_REPO_PATH}"/build/${BOARD}_${PLUGIN}/open_nic_shell
    sudo rm -rf "${OPENNIC_REPO_PATH}"/build/${BOARD}_${PLUGIN}/DESIGN_PARAMETERS
}

if [ "${arg_clean}" == "1" ]; then
    clean_build
    exit
fi

clean_build_all () {
    sudo rm -rf "${OPENNIC_REPO_PATH}"/build/${BOARD}_${PLUGIN}
}

if [ "${arg_clean_all}" == "1" ]; then
    clean_build
    clean_build_all
    exit
fi

if [ ! -d "${BUILD_DIR}/${PLUGIN}" ]; then
    echo Plugin: "${BUILD_DIR}/${PLUGIN}" does not exist.
    exit
fi

cd "${OPENNIC_REPO_PATH}/script" || exit

if [ "${arg_simulation}" == "1" ]; then
    cp -rf "${SIM_DIR}" "${OPENNIC_REPO_PATH}"
    vivado -mode batch -source "${OPENNIC_REPO_PATH}"/sim/build.tcl -tclargs -board ${BOARD} -tag ${PLUGIN} \
        -user_plugin "${BUILD_DIR}/${PLUGIN}"
elif [ "${arg_bitstream}" == "1" ]; then
    vivado -mode batch -source build.tcl -tclargs -board ${BOARD} -tag ${PLUGIN} \
        -user_plugin "${BUILD_DIR}/${PLUGIN}" -impl 1
fi

cd "${PWD}" || exit
