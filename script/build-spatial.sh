#!/usr/bin/env bash

# *************************************************************************
#
# Copyright 2021 Tushar Swamy (Stanford University),
#                Alexander Rucker (Stanford University),
#                Annus Zulfiqar (Purdue University),
#                Muhammad Shahbaz (Stanford/Purdue University)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# *************************************************************************

PWD=$(pwd)

if [ -z "${MAPREDUCE_ROOT_DIR}" ]; then
  echo "MAPREDUCE_ROOT_DIR environment variable is not set."
  exit
fi

PLUGIN=mapreduce

APP_DIR=${APP_DIR:-"${MAPREDUCE_ROOT_DIR}"/app}
PLUGIN_DIR=${PLUGIN_DIR:-"${MAPREDUCE_ROOT_DIR}"/plugin}
BUILD_DIR=${BUILD_DIR:-"${MAPREDUCE_ROOT_DIR}"/build}

SPATIAL_REPO_PATH=${SPATIAL_REPO_PATH:-"${MAPREDUCE_ROOT_DIR}"/deps/spatial}
SPATIAL_COPY_DIR=${SPATIAL_COPY_DIR:-"${SPATIAL_REPO_PATH}"/test/spatial/tests/feature/transfers}
SPATIAL_BUILD_DIR="${BUILD_DIR}"/${PLUGIN}/spatial

arg_reset_cache=0
arg_clean=0

while [[ "$#" -gt 0 ]]; do
    case $1 in
        --scala_program) arg_scala_program="$2"; shift 1 ;;
        --scala_class) arg_scala_class="$2"; shift 1 ;;
        --reset_cache) arg_reset_cache="$2"; shift 1 ;;
	      --clean) arg_clean=1 ;;
        *) echo "Unknown argument: $1"; exit 1 ;;
    esac
    shift
done

clean_build () {
    rm -rf "${BUILD_DIR}/${PLUGIN}"
    rm -rf gen
    if [ -n "${arg_scala_class}" ]; then
        rm -f "${arg_scala_class}".sim
    fi
    if [ -n "${arg_scala_program}" ]; then
        rm -f "${SPATIAL_COPY_DIR}/${arg_scala_program}"
    fi
}

if [ "${arg_clean}" == "1" ]; then
    clean_build
    exit
fi

if [ -z "${arg_scala_program}" ] || [ -z "${arg_scala_class}" ]; then
    clean_build
    exit
fi

# Copy ${PLUGIN} plugin to build directory
rm -rf "${BUILD_DIR}/${PLUGIN}"; cp -rf "${PLUGIN_DIR}/${PLUGIN}" "${BUILD_DIR}"

# Copy app to the spatial repo
rm -f "${SPATIAL_COPY_DIR}/${arg_scala_program}"; cp -f "${APP_DIR}/${arg_scala_program}" "${SPATIAL_COPY_DIR}"

echo Compiling "${arg_scala_program}":"${arg_scala_class}"

cd "${SPATIAL_REPO_PATH}" || exit

if [ "${arg_reset_cache}" == "1" ]; then
    rm -rf ~/.ivy2/cache/*
    make resources
fi

# Clean up pre-compilation
rm -rf gen

# Compiling spatial program:class
bin/spatial "${arg_scala_class}" --synth --perpetualIP --fpga=KCU1500 --noModifyStream --noBindParallels
cd gen/"${arg_scala_class}" || exit
make

cp verilog-kcu1500/* "${SPATIAL_BUILD_DIR}"
cp kcu1500.hw-resources/build/AXI4LiteToRFBridgeVerilog.v "${SPATIAL_BUILD_DIR}"

# Clean up post-compilation
rm -rf gen
rm -f "${arg_scala_class}".sim
rm -f "${SPATIAL_COPY_DIR}/${arg_scala_program}"

cd "${PWD}" || exit
