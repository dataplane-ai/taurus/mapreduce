# *************************************************************************
#
# Copyright 2021 Tushar Swamy (Stanford University),
#                Alexander Rucker (Stanford University),
#                Annus Zulfiqar (Purdue University),
#                Muhammad Shahbaz (Stanford/Purdue University)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# *************************************************************************

export MAPREDUCE_ROOT_DIR ?= ./

export SCALA_PROGRAM ?= Loopback512UnitTests.scala
export SCALA_CLASS ?= Loopback512

build-spatial:
	cd script && ./build-spatial.sh --scala_program $(SCALA_PROGRAM) --scala_class $(SCALA_CLASS)

build-spatial-rc:
	cd script && ./build-spatial.sh --scala_program $(SCALA_PROGRAM) --scala_class $(SCALA_CLASS) \
        --reset_cache 1

clean-spatial:
	cd script && ./build-spatial.sh --clean

build-fpga:
	cd script && ./build-fpga.sh --bitstream

clean-fpga:
	cd script && ./build-fpga.sh --clean

clean-fpga-all:
	cd script && ./build-fpga.sh --clean_all

build-fpga-sim:
	cd script && ./build-fpga.sh --simulation

clean-fpga-sim: clean-fpga

build-sim: build-spatial build-fpga-sim

clean-sim: clean-spatial clean-fpga-sim

build-bits: build-spatial build-fpga

clean-bits: clean-spatial

