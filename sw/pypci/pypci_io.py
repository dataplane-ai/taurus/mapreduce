#!/usr/bin/env python3

# *************************************************************************
#
# Copyright 2021 Tushar Swamy (Stanford University),
#                Alexander Rucker (Stanford University),
#                Annus Zulfiqar (Purdue University),
#                Muhammad Shahbaz (Stanford/Purdue University)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# *************************************************************************

import pypci
import struct

U250_VENDOR = 0x10ee
U250_GOLDEN_DEVICE = 0xd004
U250_DEVICE = 0x903f

u250_bar1 = None

def init(vendor, device):
    global u250_bar1
    # Open connection to the PCI device
    board = pypci.lspci(vendor=vendor, device=device)
    u250_board = board[0]
    # u250_board.vendor_id  # Read vendor id
    # u250_board.bar        # List bars
    u250_bar1 = u250_board.bar[1]
    return 0

def write(address, value):
    pypci.write(u250_bar1, address, struct.pack('<I', value))
    return 0

def read(address):
    value = pypci.read(u250_bar1, address, 4)
    return int.from_bytes(value, "little")
