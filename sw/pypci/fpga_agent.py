#!/usr/bin/env python 

# *************************************************************************
#
# Copyright 2021 Tushar Swamy (Stanford University),
#                Alexander Rucker (Stanford University),
#                Annus Zulfiqar (Purdue University),
#                Muhammad Shahbaz (Stanford/Purdue University)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# *************************************************************************

import argparse
import time
from concurrent import futures

import grpc
import pypci_io
import fpga_agent_pb2
import fpga_agent_pb2_grpc

parser = argparse.ArgumentParser(description='Taurus MapReduce FPGA Agent')
parser.add_argument('--port', help='Port', default=50051,
                    type=int, action="store")
args = parser.parse_args()


class TaurusAgentServicer(fpga_agent_pb2_grpc.TaurusAgentServicer):

    def Init(self, alveo_pcie, context):
        response = fpga_agent_pb2.Reply()
        response.value = pypci_io.init(alveo_pcie.vendor, alveo_pcie.device)
        return response

    def Write(self, request, context):
        response = fpga_agent_pb2.Reply()
        response.value = pypci_io.write(request.address, request.value)
        return response

    def Read(self, request, context):
        response = fpga_agent_pb2.Reply()
        response.value = pypci_io.read(request.address)
        return response


server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))

fpga_agent_pb2_grpc.add_TaurusAgentServicer_to_server(
    TaurusAgentServicer(), server)

print('Starting server. Listening on port {0}.'.format(args.port))
server.add_insecure_port('[::]:{0}'.format(args.port))
server.start()

try:
    while True:
        time.sleep(86400)
except KeyboardInterrupt:
    server.stop(0)
