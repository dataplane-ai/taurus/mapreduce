# Reading FPGA bars using PyPCI

### Install the `pypci` module

> Note: `pypci` requires python3 to work; therefore, we will install it using pip3.

```
pip3 install `pypci`
```

### Example python code

```
sudo ./pypci-io.py
```

### Other Utilities
```
sudo apt update
sudo apt install python3-pip
pip3 install requests argparse grpcio-tools pypci
```
