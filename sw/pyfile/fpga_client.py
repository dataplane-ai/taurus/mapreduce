#!/usr/bin/env python

# *************************************************************************
#
# Copyright 2021 Tushar Swamy (Stanford University),
#                Alexander Rucker (Stanford University),
#                Annus Zulfiqar (Purdue University),
#                Muhammad Shahbaz (Stanford/Purdue University)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# *************************************************************************

import grpc
import argparse

import fpga_agent_pb2
import fpga_agent_pb2_grpc


def init(ip='localhost', port=50051, cli_file='/tmp/cli_commands.txt'):
    global stub
    channel = grpc.insecure_channel('{0}:{1}'.format(ip, port))
    stub = fpga_agent_pb2_grpc.TaurusAgentStub(channel)

    response = stub.Init(fpga_agent_pb2.CliFile(path=cli_file))
    return response.value


def write(address, value):
    request = fpga_agent_pb2.WriteRequest(address=address, value=value)
    response = stub.Write(request)
    return response.value


def read(address):
    request = fpga_agent_pb2.ReadRequest(address=address)
    response = stub.Read(request)
    return response.value


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Taurus FPGA Client')
    parser.add_argument('--ip', help='IP', default='localhost',
                        type=str, action="store")
    parser.add_argument('--port', help='Port', default=50051,
                        type=int, action="store")
    parser.add_argument('--cli-file', help='CLI File', default='/tmp/cli_commands.txt',
                        type=str, action="store")
    parser.add_argument('--address', help='Address (e.g., 0x1000 in hex)',
                        type=str, action="store", required=True)
    parser.add_argument('--value', help='Value (e.g., 0x1000 in hex)',
                        type=str, action="store")
    args = parser.parse_args()

    init(ip=args.ip, port=args.port, cli_file=args.cli_file)

    if args.value:
        if write(int(args.address, 16), int(args.value, 16)) == 0:
            print("Write successful.")
        else:
            print("Write failed.")
    else:
        if read(int(args.address, 16)) == 0:
            print("Read successful.")
        else:
            print("Read failed.")
