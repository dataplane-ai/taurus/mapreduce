// *************************************************************************
//
// Copyright 2021 Tushar Swamy (Stanford University),
//                Alexander Rucker (Stanford University),
//                Annus Zulfiqar (Purdue University),
//                Muhammad Shahbaz (Stanford/Purdue University)
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// *************************************************************************

package spatial.tests.feature.transfers

import argon.static.Sym
import spatial.dsl._

@spatial class Loopback512 extends SpatialTest {
  import spatial.lang.{AxiStream512, AxiStream512Bus}
  // @struct case class AxiStream512(tdata: U512, tstrb: U64, tkeep: U64, tlast: Bit, tid: U8, tdest: U8, tuser: U64)
  def main(args: Array[String]): Unit = {
    // In/out buses here have type AxiStream512, meaning you can access all the axis fields in the Spatial source code (tdata, tstrb, tkeep, tlast, tid, tdest, tuser)
    //  If you only care about the tdata field, you should use type U512 instead of AxiStream512
    val inbus = StreamIn[AxiStream512](AxiStream512Bus(tid = 0, tdest = 0))
    val outbus = StreamOut[AxiStream512](AxiStream512Bus(tid = 0, tdest = 1))
    Accel {
      Stream.Foreach(*) { i =>
        val packet = inbus.value
        // val newPacket = AxiStream512(packet.tdata.bits(0::512).as[U512], packet.tstrb, packet.tkeep, packet.tlast, packet.tid, 1, 0)
        val newPacket = AxiStream512(packet.tdata, packet.tstrb, packet.tkeep, packet.tlast, packet.tid, 1, 0)
        outbus := newPacket
      }
    }
    assert(1 == 1) // Assert keeps spatial happy
  }
}

@spatial class Loopback512Mult extends SpatialTest {
  import spatial.lang.{AxiStream512, AxiStream512Bus}
  // @struct case class AxiStream512(tdata: U512, tstrb: U64, tkeep: U64, tlast: Bit, tid: U8, tdest: U8, tuser: U64)
  def main(args: Array[String]): Unit = {
    // In/out buses here have type AxiStream512, meaning you can access all the axis fields in the Spatial source code (tdata, tstrb, tkeep, tlast, tid, tdest, tuser)
    //  If you only care about the tdata field, you should use type U512 instead of AxiStream512
    val inbus = StreamIn[AxiStream512](AxiStream512Bus(tid = 0, tdest = 0))
    val outbus = StreamOut[AxiStream512](AxiStream512Bus(tid = 0, tdest = 1))
    Accel {
      Stream.Foreach(*) { i =>
        val packet = inbus.value
        val newPacket = AxiStream512((packet.tdata.bits(0::16).as[U16] * packet.tdata.bits(0::16).as[U16]).as[U512], packet.tstrb, packet.tkeep, packet.tlast, packet.tid, 1, 0)
        outbus := newPacket
      }
    }
    assert(1 == 1) // Assert keeps spatial happy
  }
}
