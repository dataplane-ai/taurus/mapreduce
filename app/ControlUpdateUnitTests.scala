// *************************************************************************
//
// Copyright 2021 Tushar Swamy (Stanford University),
//                Alexander Rucker (Stanford University),
//                Annus Zulfiqar (Purdue University),
//                Muhammad Shahbaz (Stanford/Purdue University)
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// *************************************************************************

package spatial.tests.feature.transfers

import argon.static.Sym
import spatial.dsl._

@spatial class WeightUpdate extends SpatialTest {
  import spatial.lang.{AxiStream512, AxiStream512Bus}
  override def runtimeArgs: Args = "7"
  def main(args: Array[String]): Unit = {
    val newWeights = List.tabulate(50){i => HostIO[Int]}
    val ready = HostIO[Int]
    val result = HostIO[Int]
    val inbus = StreamIn[AxiStream512](AxiStream512Bus(tid = 0, tdest = 0))
    val outbus = StreamOut[AxiStream512](AxiStream512Bus(tid = 0, tdest = 1))
    setArg(ready, 0)
    // Create HW accelerator
    // for (i <- 0 until 10) {
    //  if (i == 5) {
        newWeights.zipWithIndex.foreach{case (a,b) => a := b}
        ready := 1
    //  }
      Accel {
        Stream(*) {
          // Bizarre behavior with how this op + condition logic below get packed into a unit pipe.  Just wrap explicitly for now
          Pipe{ outbus := inbus.value } 
          Pipe{
            if (ready.value == 1) {
              result := 0
              Foreach(50 by 1){k =>
                result :+= oneHotMux(
                  List.tabulate(50){j => j == k},
                  newWeights.map(_.value)
                )
              }
            }
          }
        }
    //  }
    }
    val r = getArg(result)
    val gold = List.tabulate(50){i => i}.reduce{_+_}
    println("expected: " + gold)
    println("result: " + r)
    val cksum = gold == r
    println("PASS: " + cksum)
    assert(cksum)
  }
}

