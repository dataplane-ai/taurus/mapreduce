// *************************************************************************
//
// Copyright 2021 Tushar Swamy (Stanford University),
//                Alexander Rucker (Stanford University),
//                Annus Zulfiqar (Purdue University),
//                Muhammad Shahbaz (Stanford/Purdue University)
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// *************************************************************************
`include "open_nic_shell_macros.vh"
`timescale 1ns/1ps
module open_nic_shell_sim #(
  parameter        DEVNAME         = "tap0",
  parameter        CLI_FILENAME    = "/tmp/cli_commands.txt",
  parameter [31:0] BUILD_TIMESTAMP = 32'h01010000,
  parameter int    MIN_PKT_LEN     = 64,
  parameter int    MAX_PKT_LEN     = 1518,
  parameter int    USE_PHYS_FUNC   = 1,
  parameter int    NUM_PHYS_FUNC   = 1,
  parameter int    NUM_QUEUE       = 512,
  parameter int    NUM_CMAC_PORT   = 1
) ();

  wire                         s_axil_sim_awvalid;
  wire                  [31:0] s_axil_sim_awaddr;
  wire                         s_axil_sim_awready;
  wire                         s_axil_sim_wvalid;
  wire                  [31:0] s_axil_sim_wdata;
  wire                         s_axil_sim_wready;
  wire                         s_axil_sim_bvalid;
  wire                   [1:0] s_axil_sim_bresp;
  wire                         s_axil_sim_bready;
  wire                         s_axil_sim_arvalid;
  wire                  [31:0] s_axil_sim_araddr;
  wire                         s_axil_sim_arready;
  wire                         s_axil_sim_rvalid;
  wire                  [31:0] s_axil_sim_rdata;
  wire                   [1:0] s_axil_sim_rresp;
  wire                         s_axil_sim_rready;

  wire     [NUM_CMAC_PORT-1:0] m_axis_cmac_tx_sim_tvalid;
  wire [512*NUM_CMAC_PORT-1:0] m_axis_cmac_tx_sim_tdata;
  wire  [64*NUM_CMAC_PORT-1:0] m_axis_cmac_tx_sim_tkeep;
  wire     [NUM_CMAC_PORT-1:0] m_axis_cmac_tx_sim_tlast;
  wire     [NUM_CMAC_PORT-1:0] m_axis_cmac_tx_sim_tuser_err;
  wire     [NUM_CMAC_PORT-1:0] m_axis_cmac_tx_sim_tready;

  wire     [NUM_CMAC_PORT-1:0] s_axis_cmac_rx_sim_tvalid;
  wire [512*NUM_CMAC_PORT-1:0] s_axis_cmac_rx_sim_tdata;
  wire  [64*NUM_CMAC_PORT-1:0] s_axis_cmac_rx_sim_tkeep;
  wire     [NUM_CMAC_PORT-1:0] s_axis_cmac_rx_sim_tlast;
  wire     [NUM_CMAC_PORT-1:0] s_axis_cmac_rx_sim_tuser_err;

  wire                         powerup_rstn;
  reg                          powerup_rstn_sim;

  // Generate powerup_rstn
  initial begin
      powerup_rstn_sim = 1'b0;
      #80000 powerup_rstn_sim = 1'b1;
  end

  assign powerup_rstn = powerup_rstn_sim;

  // DUT
  open_nic_shell #(
    .BUILD_TIMESTAMP (BUILD_TIMESTAMP),
    .MIN_PKT_LEN (MIN_PKT_LEN),
    .MAX_PKT_LEN (MAX_PKT_LEN),
    .USE_PHYS_FUNC (USE_PHYS_FUNC),
    .NUM_PHYS_FUNC (NUM_PHYS_FUNC),
    .NUM_QUEUE (NUM_QUEUE),
    .NUM_CMAC_PORT (NUM_CMAC_PORT)
  ) DUT (
    .s_axil_sim_awvalid             (s_axil_sim_awvalid),
    .s_axil_sim_awaddr              (s_axil_sim_awaddr),
    .s_axil_sim_awready             (s_axil_sim_awready),
    .s_axil_sim_wvalid              (s_axil_sim_wvalid),
    .s_axil_sim_wdata               (s_axil_sim_wdata),
    .s_axil_sim_wready              (s_axil_sim_wready),
    .s_axil_sim_bvalid              (s_axil_sim_bvalid),
    .s_axil_sim_bresp               (s_axil_sim_bresp),
    .s_axil_sim_bready              (s_axil_sim_bready),
    .s_axil_sim_arvalid             (s_axil_sim_arvalid),
    .s_axil_sim_araddr              (s_axil_sim_araddr),
    .s_axil_sim_arready             (s_axil_sim_arready),
    .s_axil_sim_rvalid              (s_axil_sim_rvalid),
    .s_axil_sim_rdata               (s_axil_sim_rdata),
    .s_axil_sim_rresp               (s_axil_sim_rresp),
    .s_axil_sim_rready              (s_axil_sim_rready),

    .m_axis_cmac_tx_sim_tvalid      (m_axis_cmac_tx_sim_tvalid),
    .m_axis_cmac_tx_sim_tdata       (m_axis_cmac_tx_sim_tdata),
    .m_axis_cmac_tx_sim_tkeep       (m_axis_cmac_tx_sim_tkeep),
    .m_axis_cmac_tx_sim_tlast       (m_axis_cmac_tx_sim_tlast),
    .m_axis_cmac_tx_sim_tuser_err   (m_axis_cmac_tx_sim_tuser_err),
    .m_axis_cmac_tx_sim_tready      (m_axis_cmac_tx_sim_tready),

    .s_axis_cmac_rx_sim_tvalid      (s_axis_cmac_rx_sim_tvalid),
    .s_axis_cmac_rx_sim_tdata       (s_axis_cmac_rx_sim_tdata),
    .s_axis_cmac_rx_sim_tkeep       (s_axis_cmac_rx_sim_tkeep),
    .s_axis_cmac_rx_sim_tlast       (s_axis_cmac_rx_sim_tlast),
    .s_axis_cmac_rx_sim_tuser_err   (s_axis_cmac_rx_sim_tuser_err),

    .powerup_rstn                   (powerup_rstn)
  );

  // Control
  reg axil_aclk;

  initial begin
    axil_aclk = 1'b1;
  end

  always #4000ps axil_aclk = ~axil_aclk;

  control #(
    .CLI_FILENAME (CLI_FILENAME)
  ) control_inst (
     .axi_aclk            (axil_aclk),
     .axi_aresetn         (powerup_rstn),

     .m_axi_awaddr        (s_axil_sim_awaddr),
     .m_axi_awvalid       (s_axil_sim_awvalid),
     .m_axi_awready       (s_axil_sim_awready),
     .m_axi_wdata         (s_axil_sim_wdata),
     .m_axi_wstrb         (), // unused
     .m_axi_wvalid        (s_axil_sim_wvalid),
     .m_axi_wready        (s_axil_sim_wready),
     .m_axi_bresp         (s_axil_sim_bresp),
     .m_axi_bvalid        (s_axil_sim_bvalid),
     .m_axi_bready        (s_axil_sim_bready),
     .m_axi_araddr        (s_axil_sim_araddr),
     .m_axi_arvalid       (s_axil_sim_arvalid),
     .m_axi_arready       (s_axil_sim_arready),
     .m_axi_rdata         (s_axil_sim_rdata),
     .m_axi_rvalid        (s_axil_sim_rvalid),
     .m_axi_rready        (s_axil_sim_rready),
     .m_axi_rresp         (s_axil_sim_rresp)
  );

  // Network
  reg cmac_clk;

  initial begin
    cmac_clk = 1'b1;
    forever #1552ps cmac_clk = ~cmac_clk;
  end

  wire              s_axis_tx_tvalid_64b;
  wire      [63:0]  s_axis_tx_tdata_64b;
  wire      [7:0]   s_axis_tx_tkeep_64b;
  wire              s_axis_tx_tlast_64b;
  wire              s_axis_tx_tready_64b;
  wire              m_axis_rx_tvalid_64b;
  wire      [63:0]  m_axis_rx_tdata_64b;
  wire      [7:0]   m_axis_rx_tkeep_64b;
  wire              m_axis_rx_tlast_64b;
  wire              m_axis_rx_tready_64b;

  network #(
      .DEVNAME (DEVNAME)
  ) network_inst (
      .clock              (cmac_clk),
      .reset              (~powerup_rstn),
      // tx packets
      .net_tx_tvalid      (s_axis_tx_tvalid_64b),
      .net_tx_tready      (s_axis_tx_tready_64b),
      .net_tx_tdata       (s_axis_tx_tdata_64b),
      .net_tx_tkeep       (s_axis_tx_tkeep_64b),
      .net_tx_tlast       (s_axis_tx_tlast_64b),
      // rx packets
      .net_rx_tvalid      (m_axis_rx_tvalid_64b),
      .net_rx_tready      (m_axis_rx_tready_64b),
      .net_rx_tdata       (m_axis_rx_tdata_64b),
      .net_rx_tkeep       (m_axis_rx_tkeep_64b),
      .net_rx_tlast       (m_axis_rx_tlast_64b)
  );

  axis_tx_dwidth_converter tx_dwidth_converter_inst ( // convert: 512-bit to 64-bit @ 250 MHz
    .aclk            (cmac_clk),
    .aresetn         (powerup_rstn),
    .s_axis_tvalid   (m_axis_cmac_tx_sim_tvalid[0]),
    .s_axis_tready   (m_axis_cmac_tx_sim_tready[0]),
    .s_axis_tdata    (m_axis_cmac_tx_sim_tdata[511:0]),
    .s_axis_tkeep    (m_axis_cmac_tx_sim_tkeep[63:0]),
    .s_axis_tlast    (m_axis_cmac_tx_sim_tlast[0]),
    .m_axis_tvalid   (s_axis_tx_tvalid_64b),
    .m_axis_tready   (s_axis_tx_tready_64b),
    .m_axis_tdata    (s_axis_tx_tdata_64b),
    .m_axis_tkeep    (s_axis_tx_tkeep_64b),
    .m_axis_tlast    (s_axis_tx_tlast_64b)
  );

  axis_rx_dwidth_converter rx_dwidth_converter_inst ( // convert: 64-bit to 512-bit @ 250 MHz
    .aclk            (cmac_clk),
    .aresetn         (powerup_rstn),
    .s_axis_tvalid   (m_axis_rx_tvalid_64b),
    .s_axis_tready   (m_axis_rx_tready_64b),
    .s_axis_tdata    (m_axis_rx_tdata_64b),
    .s_axis_tkeep    (m_axis_rx_tkeep_64b),
    .s_axis_tlast    (m_axis_rx_tlast_64b),
    .m_axis_tvalid   (s_axis_cmac_rx_sim_tvalid[0]),
    .m_axis_tready   (1'b1),                         // CMAC does not handle backpressure
    .m_axis_tdata    (s_axis_cmac_rx_sim_tdata[511:0]),
    .m_axis_tkeep    (s_axis_cmac_rx_sim_tkeep[64:0]),
    .m_axis_tlast    (s_axis_cmac_rx_sim_tlast[0])
  );

  assign s_axis_cmac_rx_sim_tuser_err = 'b0;        // no CMAC errors in simulation

endmodule: open_nic_shell_sim