set tx_dwidth_converter axis_tx_dwidth_converter
create_ip -name axis_dwidth_converter -vendor xilinx.com -library ip -version 1.1 -module_name $tx_dwidth_converter -dir ${ip_build_dir}
set_property -dict {
    CONFIG.S_TDATA_NUM_BYTES {64}
    CONFIG.M_TDATA_NUM_BYTES {8}
    CONFIG.HAS_TLAST {1}
    CONFIG.HAS_TKEEP {1}
} [get_ips $tx_dwidth_converter]
