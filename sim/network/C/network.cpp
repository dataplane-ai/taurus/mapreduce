// *************************************************************************
//
// Copyright 2021 Tushar Swamy (Stanford University),
//                Alexander Rucker (Stanford University),
//                Annus Zulfiqar (Purdue University),
//                Muhammad Shahbaz (Stanford/Purdue University)
// Copyright 2020 Xilinx, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// *************************************************************************
#include <vpi_user.h>
#include <svdpi.h>

#include <stdio.h>
#include <string>
#include <unordered_map>

#include "device.hpp"

std::unordered_map<std::string, NetworkDevice*> netdev_map;

extern "C" void network_init(const char *devname)
{
    netdev_map[std::string(devname)] = new NetworkDevice(devname);
}

extern "C" void network_tick(
        const char *devname,

        unsigned char tx_valid,
        unsigned char *tx_ready,
        long long     tx_data,
        char          tx_keep,
        unsigned char tx_last,

        unsigned char *rx_valid,
        unsigned char rx_ready,
        long long     *rx_data,
        char          *rx_keep,
        unsigned char *rx_last)
{
    NetworkDevice *netdev = netdev_map[std::string(devname)];

    if (!netdev) {
        *tx_ready = 0;
        *rx_valid = 0;
        *rx_data = 0;
        *rx_keep = 0;
        *rx_last = 0;
        return;
    }

    netdev->tick_tx(tx_valid, tx_data, tx_keep, tx_last);

    *tx_ready = netdev->tx_ready();
    *rx_valid = netdev->rx_valid();
    *rx_data = netdev->rx_data();
    *rx_keep = netdev->rx_keep();
    *rx_last = netdev->rx_last();

    netdev->tick_rx(rx_ready);

}
