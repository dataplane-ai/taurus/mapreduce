// *************************************************************************
//
// Copyright 2021 Tushar Swamy (Stanford University),
//                Alexander Rucker (Stanford University),
//                Annus Zulfiqar (Purdue University),
//                Muhammad Shahbaz (Stanford/Purdue University)
// Copyright 2020 Xilinx, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// *************************************************************************
#ifndef __NET_PACKET_H__
#define __NET_PACKET_H__

#include <vpi_user.h>
#include <svdpi.h>

#include <stdlib.h>
#include <string.h>

#define ETH_MAX_WORDS 190
#define ETH_MAX_BYTES 1518

struct network_flit {
    uint64_t data;
    uint8_t keep;
    bool last;
};

struct network_packet {
  uint64_t data[ETH_MAX_WORDS];
  int len; // bytes
  int num_words;
};

static inline void init_network_packet(struct network_packet *packet)
{
    packet->len = 0;
    packet->num_words = 0;
    memset(packet->data, 0, ETH_MAX_WORDS * sizeof(uint64_t));
}

static inline void network_packet_add(network_packet *packet, uint64_t data, uint8_t keep)
{
    packet->data[packet->num_words] = data;
    packet->len += __builtin_popcount(keep);
    packet->num_words++;
}

#endif
