// *************************************************************************
//
// Copyright (c) 2020 Stanford University All rights reserved.
//
// This software was developed by
// Stanford University and the University of Cambridge Computer Laboratory
// under National Science Foundation under Grant No. CNS-0855268,
// the University of Cambridge Computer Laboratory under EPSRC INTERNET Project EP/H040536/1 and
// by the University of Cambridge Computer Laboratory under DARPA/AFRL contract FA8750-11-C-0249 ("MRC2"),
// as part of the DARPA MRC research programme.
//
// @NETFPGA_LICENSE_HEADER_START@
//
// Licensed to NetFPGA C.I.C. (NetFPGA) under one or more contributor
// license agreements.  See the NOTICE file distributed with this work for
// additional information regarding copyright ownership.  NetFPGA licenses this
// file to you under the NetFPGA Hardware-Software License, Version 1.0 (the
// "License"); you may not use this file except in compliance with the
// License.  You may obtain a copy of the License at:
//
//   http://www.netfpga-cic.org
//
// Unless required by applicable law or agreed to in writing, Work distributed
// under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
// CONDITIONS OF ANY KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations under the License.
//
// @NETFPGA_LICENSE_HEADER_END@
// *************************************************************************
// --------------------------------------------------------------------------
// Adapted from SDNet (v2019.2) example design.
// Author: Stephen Ibanez, Muhammad Shahbaz
// --------------------------------------------------------------------------

import helper_pkg::*;

module control #(
   parameter int AXI_ADDR_WIDTH         = 32,
   parameter int AXI_DATA_WIDTH         = 32,
   parameter string CLI_FILENAME       = "/tmp/cli_commands.txt"
) (
   // Clocks & Resets
   input  logic                         axi_aclk,
   input  logic                         axi_aresetn,
   // AXI4-lite interface
   output logic [AXI_ADDR_WIDTH-1:0]    m_axi_awaddr,
   output logic                         m_axi_awvalid,
   input  logic                         m_axi_awready,
   output logic [AXI_DATA_WIDTH-1:0]    m_axi_wdata,
   output logic [AXI_DATA_WIDTH/8-1:0]  m_axi_wstrb,
   output logic                         m_axi_wvalid,
   input  logic                         m_axi_wready,
   input  logic [1:0]                   m_axi_bresp,
   input  logic                         m_axi_bvalid,
   output logic                         m_axi_bready,
   output logic [AXI_ADDR_WIDTH-1:0]    m_axi_araddr,
   output logic                         m_axi_arvalid,
   input  logic                         m_axi_arready,
   input  logic [AXI_DATA_WIDTH-1:0]    m_axi_rdata,
   input  logic                         m_axi_rvalid,
   output logic                         m_axi_rready,
   input  logic [1:0]                   m_axi_rresp
);

    // --------------------------------------------------------------------------
    // AXI Lite tasks used by DPI

    localparam int START_DELAY            = 200; // cycles
    localparam int CLI_FILE_SAMPLE_PERIOD = 100; // cycles

    localparam AXI_DEBUG        = 0;
    localparam logic [1:0] OKAY = 2'b00;

    localparam int AXIS_WRITE_TIMEOUT = 20;
    localparam int AXIS_READ_TIMEOUT  = 50;

    logic [1:0] m_axi_bresp_fifo[$];
    logic [1:0] m_axi_rresp_fifo[$];

    // Register read/write functions used by SDNet DPI drivers
    export "DPI-C" task axi_lite_wr;
    export "DPI-C" task axi_lite_rd;

    initial begin
         m_axi_awaddr  <= 32'b0;
         m_axi_awvalid <= 1'b0;
         m_axi_wdata   <= 32'b0;
         m_axi_wstrb   <= 4'b0;
         m_axi_wvalid  <= 1'b0;
         m_axi_bready  <= 1'b0;
         m_axi_araddr  <= 32'b0;
         m_axi_arvalid <= 1'b0;
         m_axi_rready  <= 1'b0;
     end

    // AXI lite 32-bit write
    task axi_lite_wr (input int address, input int data);
        logic addr_done;
        logic data_done;

        @(posedge axi_aclk) begin
            m_axi_awaddr  <= address;
            m_axi_awvalid <= 1'b1;
            m_axi_wdata   <= data;
            m_axi_wstrb   <= 4'b1111;
            m_axi_wvalid  <= 1'b1;
            addr_done     <= m_axi_awready;
            data_done     <= m_axi_wready;
            m_axi_bresp_fifo.push_back(OKAY);
        end

        if (AXI_DEBUG)
            $display("** Info: Write address = 0x%h, data = 0x%h   %t ps", address, data, $time);

        for (int i = 0; i < AXIS_WRITE_TIMEOUT; i++) begin
            @(posedge axi_aclk) begin
                if (addr_done || m_axi_awready) begin
                    m_axi_awvalid <= 1'b0;
                    addr_done     <= 1'b1;
                end
                if (data_done || m_axi_wready) begin
                    m_axi_wvalid <= 1'b0;
                    data_done    <= 1'b1;
                end
                if (addr_done && data_done)
                   break;
            end
        end

        if (!(addr_done && data_done))
           $fatal(1, "** Error: Time-out for write data=%x to address=%x   %t ps", address, data, $time);
    endtask

    // AXI lite 32-bit read
    task axi_lite_rd (input int address, inout int data);
        logic addr_done;
        logic data_done;

        @(posedge axi_aclk) begin
            m_axi_araddr  <= address;
            m_axi_arvalid <= 1'b1;
            addr_done     <= m_axi_arready;
            data_done     <= m_axi_rvalid;
            m_axi_rready  <= 1'b1;
            m_axi_rresp_fifo.push_back(OKAY);
        end

        for (int i = 0; i < AXIS_READ_TIMEOUT; i++) begin
            @(posedge axi_aclk) begin
                if (addr_done || m_axi_arready) begin
                    m_axi_arvalid <= 1'b0;
                end
                if (data_done || m_axi_rvalid) begin
                    data <= m_axi_rdata;
                    m_axi_rready <= 1'b0;
                end
                if (addr_done && data_done) begin
                    m_axi_arvalid <= 1'b0;
                    m_axi_rready  <= 1'b0;
                    break;
                end
                addr_done <= addr_done | m_axi_arready;
                data_done <= data_done | m_axi_rvalid;
            end
        end

        if (!(addr_done && data_done))
            $fatal(1, "Time-out for read at address = 0x%h   %t ps", address, $time);

        if (AXI_DEBUG)
            $display("** Info: Read address  = 0x%h, data = 0x%h   %t ps", address, data, $time);
    endtask

    // AXI4-lite bresp check
    logic [1:0] bresp_expect;
    int   mgmt_wr_err_cnt;
    int   mgmt_wr_ok_cnt;

    always @(posedge axi_aclk) begin : bresp_check
        if (!axi_aresetn) begin
            mgmt_wr_err_cnt <= '0;
            mgmt_wr_ok_cnt  <= '0;
        end
        else begin
            m_axi_bready <= 1'b1;
            if (m_axi_bvalid) begin
                if (m_axi_bready) begin
                    m_axi_bready <= 1'b0;
                    bresp_expect = m_axi_bresp_fifo.pop_front();
                    if (m_axi_bresp != bresp_expect) begin
                        $fatal(1, "Incorrect correct data %2b, expected %2b", m_axi_bresp, bresp_expect);
                        mgmt_wr_err_cnt <= mgmt_wr_err_cnt+1;
                    end
                    else begin
                        mgmt_wr_ok_cnt <= mgmt_wr_ok_cnt+1;
                    end
                end
                else begin
                    m_axi_bready <= 1'b1;
                end
            end
        end
    end

    // AXI4-lite rresp check
    logic [1:0] rresp_expect;
    int   mgmt_rd_err_cnt;
    int   mgmt_rd_ok_cnt;

    always @(posedge axi_aclk) begin : rresp_check
        if (!axi_aresetn) begin
            mgmt_rd_err_cnt <= '0;
            mgmt_rd_ok_cnt  <= '0;
        end
        else begin
            if (m_axi_rvalid && m_axi_rready) begin
                rresp_expect = m_axi_rresp_fifo.pop_front();
                if (m_axi_rresp != rresp_expect) begin
                    $fatal(1, "Incorrect correct data %2b, expected %2b", m_axi_rresp, rresp_expect);
                    mgmt_rd_err_cnt <= mgmt_rd_err_cnt + 1;
                end
                else begin
                    mgmt_rd_ok_cnt <= mgmt_rd_ok_cnt + 1;
                end
            end
        end
    end

    // -----------------------------------------------------------------------
    // CLI command loop
    // -----------------------------------------------------------------------

    strArray cmd_line;
    string str_line;
    string command;
    int fh, cmd_address, cmd_data;
    int cmd_count, last_cmd;

    initial begin

        // Wait for reset signal to start
        @ (posedge axi_aresetn);
        for (int d=0; d < START_DELAY; d++) @ (posedge axi_aclk);

        // Create/clear CLI_FILENAME file
        fh = $fopen($sformatf(CLI_FILENAME), "w");
        if (!fh) begin
            $fatal(1, "** Error: Failed to create/clear file '%s'", CLI_FILENAME);
        end
        $fwrite(fh, "");
        $fclose(fh);

        last_cmd = 0;

        /* Every CLI_FILE_SAMPLE_PERIOD cycles, try to open the CLI_FILENAME file and
         * look for new commands that we haven't executed yet.
         */
        while (1) begin
            // Try to open CLI commands file
            fh = $fopen($sformatf(CLI_FILENAME), "r");
            if (!fh) begin
                $display("WARNING: could not open file '%s' for reading", CLI_FILENAME);
                continue;
            end

            cmd_count = 0;

            // read file lines
            while(!$feof(fh)) begin
                if($fgets(str_line, fh)) begin

                    // remove '\n' at the end of line
                    while (str_line[str_line.len()-1] == "\n")
                        str_line = str_line.substr(0, str_line.len()-2);

                    // ignore comments and empty lines
                    if (str_line[0] == "%"  ||
                        str_line[0] == "#"  ||
                        str_line[0] == "\n" ||
                        str_line.len() == 0)
                        continue;

                    // skip over the commands that we've already executed
                    if (cmd_count < last_cmd) begin
                        cmd_count++;
                        continue;
                    end

                    cmd_count++;
                    last_cmd++;

                    // split line and parse command
                    cmd_line = split(str_line, " ");
                    command = cmd_line[0];
                    case (command)
                         // write_addr <address> <data>
                         "write_addr" : begin
                             cmd_address = str2hex(cmd_line[1]);
                             cmd_data = str2hex(cmd_line[2]);
                             axi_lite_wr(cmd_address, cmd_data);
                             $display("** Info: Writing data: 0x%x to address: 0x%x",
                                      cmd_data, cmd_address);
                         end

                         // read_addr <address>
                         "read_addr" : begin
                             cmd_address = str2hex(cmd_line[1]);
                             axi_lite_rd(cmd_address, cmd_data);
                             $display("** Info: Reading data: 0x%x from address: 0x%x",
                                      cmd_data, cmd_address);
                         end

                         // exit
                         "exit" : begin
                             break;
                         end

                         // ignore invalid commands
                         default : begin
                             $display("** Info: Ignoring invalid command '%0s'", command);
                             continue;
                         end

                    endcase
                end
            end

            // We've read all the lines, now close the file.
            $fclose(fh);

            // Wait a while before checking the CLI_FILENAME file again
            for (int d=0; d < CLI_FILE_SAMPLE_PERIOD; d++) @ (posedge axi_aclk);
        end

        // test report
        $display("** Info: Terminating CLI.");
    end

endmodule
