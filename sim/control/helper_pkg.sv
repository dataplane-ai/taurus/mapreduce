// --------------------------------------------------------------------------
//   This file is owned and controlled by Xilinx and must be used solely
//   for design, simulation, implementation and creation of design files
//   limited to Xilinx devices or technologies. Use with non-Xilinx
//   devices or technologies is expressly prohibited and immediately
//   terminates your license.
//
//   XILINX IS PROVIDING THIS DESIGN, CODE, OR INFORMATION 'AS IS' SOLELY
//   FOR USE IN DEVELOPING PROGRAMS AND SOLUTIONS FOR XILINX DEVICES.  BY
//   PROVIDING THIS DESIGN, CODE, OR INFORMATION AS ONE POSSIBLE
//   IMPLEMENTATION OF THIS FEATURE, APPLICATION OR STANDARD, XILINX IS
//   MAKING NO REPRESENTATION THAT THIS IMPLEMENTATION IS FREE FROM ANY
//   CLAIMS OF INFRINGEMENT, AND YOU ARE RESPONSIBLE FOR OBTAINING ANY
//   RIGHTS YOU MAY REQUIRE FOR YOUR IMPLEMENTATION.  XILINX EXPRESSLY
//   DISCLAIMS ANY WARRANTY WHATSOEVER WITH RESPECT TO THE ADEQUACY OF THE
//   IMPLEMENTATION, INCLUDING BUT NOT LIMITED TO ANY WARRANTIES OR
//   REPRESENTATIONS THAT THIS IMPLEMENTATION IS FREE FROM CLAIMS OF
//   INFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
//   PARTICULAR PURPOSE.
//
//   Xilinx products are not intended for use in life support appliances,
//   devices, or systems.  Use in such applications are expressly
//   prohibited.
//
//   (c) Copyright 1995-2018 Xilinx, Inc.
//   All rights reserved.
// --------------------------------------------------------------------------

package helper_pkg;

   // -----------------------------------------------------------------------
   // Custom data types

   typedef string strArray[$];
   typedef bit [1023:0] bitArray;

   // -----------------------------------------------------------------------
   // Functions

   // split string using delimiter
   function automatic strArray split (
      input string str_in,      // input string
      input byte   delim = " "  // delimited character. Default ' ' (white space)
   );

      int str_idx = 0;
      string str_tmp = "";
      strArray str_out;

      for (int i = 0; i <= str_in.len(); i++) begin
          if (str_in[i] == delim || i == str_in.len()) begin
            if (str_tmp.len() > 0) begin
              str_out[str_idx] = str_tmp;
              str_tmp = "";
              str_idx++;
            end
          end else begin
            str_tmp = {str_tmp, str_in[i]};
          end
      end

      return str_out;
    endfunction

   // check if string contains character
   function automatic bit contains (
       input string str,  // input string
       input byte   char  // character to find
   );

       for (int i = 0; i <= str.len(); i++) begin
          if (str[i] == char) begin
             return 1;
          end
       end

      return 0;
   endfunction

   // interprets the string as hexadecimal
   function automatic bitArray str2hex (
       input string str // string with hex value. May or may not contain '0x' at the beginning
   );

       string char;
       bit [3:0] nibble;
       bitArray hex = '0;

       if (str.substr(0, 2) == "0x")
           str = str.substr(3, str.len()-1);

       for (int i = 0; i < str.len(); i++) begin
          char   = str.substr(i, i);
          nibble = char.atohex();
          hex    = {hex[$high(hex)-$size(nibble):0], nibble};
      end

       return hex;
   endfunction

endpackage
